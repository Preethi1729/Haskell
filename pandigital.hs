--project euler problem 38
import Data.List
 
mult n i vs 
    | length (concat vs) >= 9 = concat vs
    | otherwise               = mult n (i+1) (vs ++ [show (n * i)])
 
pandigitalNum :: Int
pandigitalNum = maximum . map read . filter ((['1'..'9'] ==) . sort) 
               $ [mult n 1 [] | n <- [2..9999]]

main = do
    print pandigitalNum