-- project euler problem 6
square_sum = (sum [1..100])^2 - sum (map (^2) [1..100])

main = do
    print square_sum