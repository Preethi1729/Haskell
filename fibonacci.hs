-- project euler problem 2
main = do
        let fibs = 1 : 1 : zipWith (+) fibs (tail fibs)
        print(sum (takeWhile(<4000000)[ x | x <- fibs,rem x 2 == 0]))
